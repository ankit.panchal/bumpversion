# Bumpversion CI/CD Component

This CI/CD component runs `bump2version` for automated version management in Python projects. It bumps the version as per the commit message or a default bump type.

## Usage

To use this component, add the following to your `.gitlab-ci.yml`:

```yaml
include:
  - component: ${CI_SERVER_HOST}/path/to/bumpversion-component@~latest
```

You can override the default stage, image, and version bump type.


```yaml
include:
  - component: ${CI_SERVER_HOST}/path/to/bumpversion-component@~latest
    inputs:
      stage: bump_version
      image: python:3.9
      bump_type: minor
```

### Inputs

| Input     | Default value | Description                              |
|-----------|---------------|------------------------------------------|
| `stage`   | `bump_version`| The stage where the job will be executed |
| `image`   | `python:3.9`  | Docker image to use for the job          |
| `bump_type` | `patch`      | Default version bump type                |


### Requirements
- A .bumpversion.cfg file in your project.
- requirements-dev.txt should include bump2version.
- GitLab CI/CD variables: GITLAB_USER_EMAIL, GITLAB_USER_NAME, and PROJECT_ACCESS_TOKEN.